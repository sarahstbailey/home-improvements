The scope of home improvement is to make our lives easier and to make us love our home better. With good weather and aging babies, time to get home stuff done!

```mermaid
graph TD
	A[House] -->|Get money| B(Go shopping)
	B --> C{Upgrades}
	C -->|1st Floor| D[Add chandelier]
	C -->|2nd Floor| E[Repair drawer]
	C -->|Basement| F[N/A]
  C -->|Outside| G[Mulch new trees]

```

